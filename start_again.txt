<br><b>Start Again</b> <br><br>
And I remember everything,<br>
Everything I loved,<br>
I gave it away like it wasn't enough<br>
All the words I said and all you forgive<br>
How could I hurt you again?<br><br>

What if I let you in?<br>
What if I make it right it?<br>
What if I give it up?<br>
What if I want to try?<br>
What if you take a chance?<br>
What if I learn to love?<br>
What if, what if we start again?<br><br>

[Chorus]<br>
On this time<br>
I can make it right<br>
With one more try<br>
Can we start again?<br>
In my eyes,<br>
You can see it now,<br>
Can we start again, can we start again?!?<br><br>

Emptiness inside me, wonder if you see<br>
It's my mistake and it's hurting me<br>
I known where we've been<br>
How did we get so far?<br>
What if, what if we start again?<br><br>

[Chorus]<br><br>

I'm lost inside the pain I feel without you,<br>
I can't stop holding on, I need you with me!!!<br>
I'm trapped inside the pain<br><br>
Can we ever start again?<br>
I'm lost without you!!!<br><br>

One more try,<br>
Can we start again?<br>
In my eyes, can you forgive me now?<br>
(Can we start again?)<br>
Can we start again (one more try?)<br>
Can we start again?<br>
Can we start again (can you forgive me?)<br>
Can we start again?<br><br>
